import React from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom'
import './App.css';

import MainNavigation from './components/MainNavigation/MainNavigation';
import ToDoList from './components/ToDoList/ToDoList';

const app = () => {
  const load = (
    <React.Fragment>
      <MainNavigation />
      <main className="main-content">
        <Redirect from="/" to="/home" exact />
        <Route path="/home" component={ToDoList} />
      </main>
    </React.Fragment>
  )
  return (
    <BrowserRouter>
      <React.Fragment>
        {load}
      </React.Fragment>
    </BrowserRouter>
  );
}

export default app;
