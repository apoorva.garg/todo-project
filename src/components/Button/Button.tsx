import React, { ReactNode } from 'react'

import './Button.css';

type Props = {
    clicked: () => void,
    children: ReactNode
};

const button = (props: Props) => (
    <button className="Button"
        onClick={props.clicked} type="button">{props.children}</button>
)

export default button