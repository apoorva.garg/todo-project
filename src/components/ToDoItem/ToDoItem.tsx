import React from 'react';

import './ToDoItem.css';

type Props = {
    clicked: (item: any) => void
};

const ToDoItem = (props: Props) => {
    const items = JSON.parse(localStorage.getItem("Items") || '[]')

    return (
        <ul className="todo-items">
            {
                items.map((item: any, index: number) => (
                    <div className="todo-item" key={index}>
                        <li onClick={() => props.clicked(item)} className={item.status === 'Completed' ? 'complete-style' : ''}>{item.value}</li>
                        <span>Status: {item.status}</span>
                    </div>
                ))
            }
        </ul>
    );
}

export default ToDoItem