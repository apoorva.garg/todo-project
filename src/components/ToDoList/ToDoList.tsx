import React, { useRef, useState } from 'react'

import './ToDoList.css';

import Input from '../Input/Input';
import Button from '../Button/Button';
import ToDoItem from '../ToDoItem/ToDoItem';
import ToDoItemsCounter from '../ToDoItemsCounter/ToDoItemsCounter';


let ITEMS_ARRAY: any = JSON.parse(localStorage.getItem("Items") || '[]');

const ToDoList = () => {
    const itemRef = useRef<HTMLInputElement>(null!)
    const [currentItems, setItems] = useState([] as any)

    const addItem = () => {
        const itemValue = itemRef.current.value;
        let item = {
            status: 'Pending',
            value: itemValue
        }
        ITEMS_ARRAY.push(item)
        setLocalStorage();
        itemRef.current.value = ''
    }

    const setLocalStorage = () => {
        localStorage.setItem('Items', JSON.stringify(ITEMS_ARRAY))
        const items = JSON.parse(localStorage.getItem("Items") || '[]')
        setItems(items)
    }

    const deleteToDo = (item: any) => {
        const oldItemSrray = [...ITEMS_ARRAY]
        oldItemSrray.map((ele: any) => {
            if (ele.value === item.value) {
                if (ele.status === 'Completed') {
                    ele.status = 'Pending'
                } else if (ele.status === 'Pending') {
                    ele.status = 'Completed'
                }
            }
            return ele
        })
        setLocalStorage()
    }

    let loadedItems = <p>There is no items right now, Please start adding it.</p>

    if (currentItems.length > 0 || ITEMS_ARRAY.length > 0) {
        loadedItems = (
            <ToDoItem clicked={deleteToDo} />
        )
    }

    return (
        <div className="main-container">
            <div>
                <ToDoItemsCounter />
            </div>
            <form>
                <Input label="Enter To Do Item" type="text" placeholder="Enter Item" id="item_id" refel={itemRef}></Input>
                <div style={{ textAlign: 'center' }}>
                    <Button clicked={addItem}>Add Item</Button>
                </div>
            </form>
            {loadedItems}
        </div >
    );
}

export default ToDoList