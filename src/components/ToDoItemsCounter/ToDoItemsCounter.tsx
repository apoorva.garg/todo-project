import React, { useEffect, useState } from 'react'

import './ToDoItemsCounter.css'


const ToDoItemsCounter = () => {
    const items = JSON.parse(localStorage.getItem("Items") || '[]')

    const [counter, setcounter] = useState(0)

    useEffect(() => {
        let count = 0
        items.map((ele: any) => {
            if (ele.status === 'Pending') {
                count += 1
            }
        })
        setcounter(count)
    })

    return (
        <div style={{ textAlign: 'center' }}>
            <p style={{fontSize: '18px'}}>Current Pending Items: <span className="counter">{counter}</span></p>
        </div>
    )
}

export default ToDoItemsCounter