import React from 'react'

import './Input.css';

type Props = {
    label: string,
    type: string,
    placeholder: string,
    id: string,
    refel: React.RefObject<HTMLInputElement>
};

const input = (props: Props) => {
    return (
        <div className="Input">
            <label className="Label">{props.label}</label>
            <input className="InputElement" {...props} ref={props.refel} />
        </div>
    );
}

export default input