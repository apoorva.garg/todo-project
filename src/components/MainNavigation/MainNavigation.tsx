import React from 'react';

import './MainNavigation.css';

const mainNavigation = () => {
    return(
        <header className="main-navigation">
            <div className="main-navigation-title">
                <h1>ToDo List</h1>
            </div>
        </header>
    );
}

export default mainNavigation
